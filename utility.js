module.exports = (bot, cmd, message) => {
  if(!cmd[1]) {
    message.channel.sendMessage("Specify an argument.")
  }

  if(cmd[1] == "nav" || cmd[1] == "space" || cmd[1] == "navmap") {
    message.channel.sendMessage("**Sirius Nav Map:** http://space.discoverygc.com/navmap/")
  }

  if(cmd[1] == "rules") {
    message.channel.sendMessage("**Server Rules:** http://discoverygc.com/forums/showthread.php?tid=2334")
  }

  if(cmd[1] == "laws" || cmd[1] == "los") {
    message.channel.sendMessage("**Laws of Sirius:** http://discoverygc.com/forums/showthread.php?tid=8584")
  }

  if(cmd[1] == "factionrules") {
    message.channel.sendMessage("**Faction Creation Rules:** http://discoverygc.com/forums/showthread.php?tid=1746")
  }
}
