const HELPTEXT = `Hello!

I'm Eli and I'm a Discovery Freelancer-themed Discord bot. All of my commands start with the character **&**.

__**Plugins:**__
`

const HELP = `**help:**
\`&help\` - Displays this list.
\`&help about\` - Sends information about the bot.
\`&help [plugin]\` - Displays help about a particular plugin.

**Available plugins:** \`help\`, \`time\`, \`id\`, \`utility\`
`

const ABOUT = `.

__**Eli** 1.0__

**Author**: MrHudson / Corile
**Website**: Not available yet.

**Source code**: http://gitlab.com/mrhudson/eli
**Discovery Freelancer**: http://discoverygc.com
`

const TIME = `**time:**
\`&time\` - Displays the current server time.
\`&time me <timezone>\` - Adds your timezone to my database. This is relevant in further time commands. The [timezone] has to be in the tzdatabase format (e.g. **Europe/Warsaw** or **America/New_York**) or use the shorthand (e.g. **CET** or **EST**).
\`&time check <username...>\` - Check the current time for a particular user.
\`&time convert <timestamp> <timezone> [offset]\` - Converts a timestamp to a particular timezone. \`timestamp\` is the time in the format HH:mm (24-hour clock) or *now*. \`timezone\` is the timezone you want to convert to. \`offset\` is either *today*, *tomorrow*, *yesterday* or an integer value representing a time offset. If empty, defaults to *today*.
`

const ID = `**id:**
\`&id nick <nickname>\` - Displays the information about a particular ID by their nickname.
\`&id search <parameter> <value>\` - Searches the ID database based on the selected conditions. Parameter can be *nick, name, type, desc, lines, zoi, ships*. Value is **case sensitive**.

This plugin uses the Stardust database: http://corile.ga/stardust.`

const UTILITY = `**utility:**

\`&u\` and \`&utility\` are interchangeable.

\`&u nav\` or \`&u space\` or \`&u navmap\` - Links the nav map.
\`&u rules\` - Links the server rules.
\`&u laws\` or \`&u los\` - Links the Laws of Sirius.
\`&u factionrules\` - Links the faction creation rules.
`

module.exports = (bot, cmd, message) => {
  let user = message.author
  //&help
  if(!cmd[1]) {
    user.sendMessage(HELPTEXT+HELP)
    message.channel.sendMessage(`Sent you the help on PM.`)
  }
  if(cmd[1] == "about") user.sendMessage(ABOUT)
  if(cmd[1] == "help") user.sendMessage(HELP)
  if(cmd[1] == "time") user.sendMessage(TIME)
  if(cmd[1] == "id") user.sendMessage(ID)
  if(cmd[1] == "utility") user.sendMessage(UTILITY)
}
